![logo_fr.png](logo_fr.png)

## Introduction

Merci d'avoir téléchargé la traduction FR de Intersect Engine Beta 6 !

/!\ La traduction suit toujours son cours, soyez à l'affut des prochaines maj.

Concernant l'installation de la traduction française, c'est très simple.

## Utilisation

Il vous suffit de remplacer les fichiers originaux par les nouveaux : 

- client_strings.json et editor_strings.json dans client and editor/resources
- server_strings.json dans server/resources
- PasswordReset.html dans server/resources/notifications 

Et voila le tour est joué, maintenant votre moteur de jeu de orpg 2d préféré devrait être en français. :D

/!\ En revanche, faites attention à ne pas les confondre car ils ont le même nom.

## Remerciement

Je tiens à remercier Glaz et caranille pour leur contribution à la construction de la traduction française d'Intersect en général mais aussi pour m'avoir aidè à traduire et à gagner un peu de temps. *oof*

Amusez-vous et bonne chance dans la création de votre jeu !

## Crédits :
- Glaz (trad fr Beta 5)
- caranille (trad fr Beta 4)
- faller-magie (trad fr Beta 6)

Copyright (C) 2019 Ascension Game Dev, Tous droits réservés.